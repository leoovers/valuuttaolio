<?php
require_once 'class/Currency.php'
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Valuuttalaskin</title>
</head>

<body>
    <h3>Valuuttalaskin</h3>
    <?php
    $result = $currencyObject = new Currency();
    if ($_SERVER['REQUEST_METHOD']==='POST') {
        $money = filter_input(INPUT_POST,'amount',FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
        $currencyObject->setToCurrency('GBP');
        $result = $currencyObject->calculate($money);
    }
    ?>
    <form action=<?php print $_SERVER['PHP_SELF']; ?> method="post">
        <div>
            <label>EUR</label>
            <input type="number" name="amount" step="0.01" value="<?php print $amount ?>">
        </div>
        <div>
            <label>GBP</label>
            <output><?php printf("%.2f",$result); ?></output>
        </div>
        <button>Laske</button>
    </form>
</body>

</html>